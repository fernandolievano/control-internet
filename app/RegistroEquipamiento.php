<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroEquipamiento extends Model
{
    protected $fillable = ['estado', 'monto', 'cliente_id', 'descripcion'];

    public function cliente()
    {
      return $this->belongsTo(Cliente::class);
    }
}
