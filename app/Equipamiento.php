<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipamiento extends Model
{
    protected $fillable = ['equipo', 'precio', 'tipo', 'cantidad', 'unidad'];

}
