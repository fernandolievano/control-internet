<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = ['precio', 'velocidad'];

    public function clientes()
    {
        return $this->hasMany(Cliente::class);
    }

    public function registro()
    {
      return $this->hasMany(RegistroPlan::class);
    }
}
