<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'mes', 'nombre', 'apellido', 'email', 'telefono', 'direccion', 'plan_id', 'inactivo'
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function registros_plan()
    {
      return $this->hasMany(RegistroPlan::class);
    }

    public function registros_equipamiento()
    {
      return $this->hasMany(RegistroEquipamiento::class);
    }
}
