<?php

namespace App\Http\Controllers;

use App\Plan;
use Illuminate\Http\Request;

class PlanController extends Controller
{

    public function index()
    {
        return view('planes');
    }

    public function planes()
    {
        $planes = Plan::with('clientes')->withCount('clientes')->get();
        return response()->json($planes);
    }

    public function store(Request $request)
    {
        $rules = [
            'precio' => 'required',
            'velocidad' => 'required'
        ];
        $messages = [
            'precio.required' => 'Debes especificar el precio del nuevo plan',
            'velocidad.required' => 'Debes especificar la velocidad del nuevo plan'
        ];

        $this->validate($request, $rules, $messages);

        Plan::create($request->all());

        return "ok";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(Plan $plan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(Plan $plan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plan $plan)
    {
        //
    }

    public function destroy($id)
    {
        $plan = Plan::find($id);
        $plan->delete();
        
        return;
    }
}
