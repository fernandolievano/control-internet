<?php

namespace App\Http\Controllers;

use App\Equipamiento;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EquipamientoController extends Controller
{
    public function index()
    {
        return view('equipamientos');
    }

    public function equipos()
    {
      $equipamiento = Equipamiento::all();
      return response()->json($equipamiento);
    }

    public function store(Request $request)
    {
        $equipo = new Equipamiento;
        $equipo->equipo     = $request->get('equipo');
        $equipo->precio     = $request->get('precio');
        $equipo->tipo       = $request->get('tipo');
        $equipo->unidad     = $request->get('unidad');
        $equipo->cantidad   = $request->get('cantidad');
        $equipo->save();

         return response($equipo->jsonSerialize(), Response::HTTP_CREATED);
    }

    public function update(Request $request, $id)
    {
        $equipo = Equipamiento::find($id);

        $equipo->equipo     = $request->get('equipo');
        $equipo->precio     = $request->get('precio');
        $equipo->unidad     = $request->get('unidad');
        $equipo->tipo       = $request->get('tipo');
        $equipo->cantidad   = $request->get('cantidad');
        $equipo->update();

        return "ok";
    }

    
    public function destroy($id)
    {
        $equipamiento = Equipamiento::find($id);
        $equipamiento->delete();

        return "ok";
    }
}
