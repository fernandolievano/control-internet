<?php

namespace App\Http\Controllers;

use App\RegistroPlan;
use Illuminate\Http\Request;

class RegistroPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegistroPlan  $registroPlan
     * @return \Illuminate\Http\Response
     */
    public function show(RegistroPlan $registroPlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegistroPlan  $registroPlan
     * @return \Illuminate\Http\Response
     */
    public function edit(RegistroPlan $registroPlan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegistroPlan  $registroPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistroPlan $registroPlan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegistroPlan  $registroPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistroPlan $registroPlan)
    {
        //
    }
}
