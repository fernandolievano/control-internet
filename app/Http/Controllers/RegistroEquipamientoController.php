<?php

namespace App\Http\Controllers;

use App\RegistroEquipamiento;
use Illuminate\Http\Request;

class RegistroEquipamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegistroEquipamiento  $registroEquipamiento
     * @return \Illuminate\Http\Response
     */
    public function show(RegistroEquipamiento $registroEquipamiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegistroEquipamiento  $registroEquipamiento
     * @return \Illuminate\Http\Response
     */
    public function edit(RegistroEquipamiento $registroEquipamiento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegistroEquipamiento  $registroEquipamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistroEquipamiento $registroEquipamiento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegistroEquipamiento  $registroEquipamiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegistroEquipamiento $registroEquipamiento)
    {
        //
    }
}
