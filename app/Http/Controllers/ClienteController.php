<?php

namespace App\Http\Controllers;

use App\User;
use App\Plan;
use App\Cliente;
use App\Equipamiento;
use App\RegistroPlan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ClienteController extends Controller
{

    public function admin()
    {
        User::create([
            'username' => "admin",
            'password' => Hash::make("password"),
        ]);

        return "you're registered";
    }

    public function index()
    {
        return view('clientes');
    }

    public function clientes()

    {

        $clientes = Cliente::with([
            'plan:id,velocidad',
        ])->get();

        return $clientes;
    }

    public function store(Request $request)
    {
        $plan_ID    = $request->get('planID');

        $fecha = Carbon::now();
        $mes   = $fecha->month;

        $cliente = new Cliente;
        $cliente->nombre            = $request->get('nombre');
        $cliente->apellido          = $request->get('apellido');
        $cliente->direccion         = $request->get('direccion');
        $cliente->telefono          = $request->get('telefono');
        $cliente->inactivo          = false;
        $cliente->email             = $request->get('email');//nullable
        $cliente->plan_id           = $plan_ID;
        $cliente->save();

        $cliente_ID = $cliente->id;

        //Registro Plan
        $registro_plan = new RegistroPlan;
        $registro_plan->estado      = 'Sin pagar';
        $registro_plan->mes         = $mes;
        $registro_plan->cliente_id  = $cliente_ID;
        $registro_plan->plan_id     = $plan_ID;
        $registro_plan->save();

        //Registro Equipamiento
        if ($request->has('equipamiento')) {
            $equipos    = $request->get('equipamiento');
            $cuotas     = $request->get('cuotas');

            $cant_e = count($equipos);
            $total  = 0;

            $descripcion = 'Compra/Financiación de los siguientes elementos: ';

            for ($i=0; $i < $cant_e ; $i++) {
                $id = $equipos[$i]['id'];

                if ($id > 0) {
                    $equipo     = Equipamiento::find($id);
                    $cantidad   = $equipos[$i]['cantidad'];

                    $total += $equipo->precio * $cantidad;

                    if ($i == $cant_e-1) {
                        $descripcion .=  $cantidad .'/'. $equipo->unidad . ' de ' . $equipo->equipo . '.';
                    } else {
                        $descripcion .=  $cantidad .'/'. $equipo->unidad . ' de ' . $equipo->equipo . ', ';
                    }
                }
            }

            //hacer calculo
            // $monto

            for ($i=0; $i < $cuotas; $i++) {
                $registro_equipamiento = new RegistroEquipamiento;
                $registro_equipamiento->estado = 'Sin pagar';
                $registro_equipamiento->monto = $monto;
                $registro_equipamiento->descripcion = $descripcion;
                $registro_equipamiento->cliente_id = $cliente_ID;
                $registro_equipamiento->save();
            }
        }
    }


    public function show(Cliente $cliente)
    {
        //
    }


    public function edit(Cliente $cliente)
    {
        //
    }


    public function update(Request $request, Cliente $cliente)
    {
        //
    }


    public function destroy($id)
    {
        $cliente = Cliente::find($id);
        $cliente->delete();

        return;
    }
}
