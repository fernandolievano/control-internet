<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroPlan extends Model
{
  protected $fillable = ['estado', 'cliente_id', 'mes', 'plan_id'];

    public function cliente()
    {
      return $this->belongsTo(Cliente::class);
    }

    public function plan()
    {
      return $this->belongsTo(Plan::class);
    }
}
