(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/EquipoNuevo.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/EquipoNuevo.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'EquipoNuevo',
  data: function data() {
    return {
      equipamiento: {
        equipo: '',
        precio: '',
        tipo: '',
        cantidad: '',
        unidad: ''
      }
    };
  },
  methods: {
    nuevoEquipo: function nuevoEquipo() {
      var _this = this;

      //formato para guardar
      this.equipamiento.precio = this.equipamiento.precio.toFixed(2); //formato para mostrar

      var formatoPrecio = this.equipamiento.precio.replace(/\d(?=(\d{3})+\.)/g, '$&,');
      formatoPrecio = '$' + formatoPrecio;
      var params = Object.assign({}, this.equipamiento);
      var message = 'Equipo: ' + this.equipamiento.equipo + ', Precio: ' + formatoPrecio + ', Tipo: ' + this.equipamiento.tipo + ', Unidad: ' + this.equipamiento.unidad + ', Cantidad: ' + this.equipamiento.cantidad;
      Swal.fire({
        title: '¿Los datos son correctos?',
        text: message,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, son correctos',
        cancelButtonText: 'Cancelar'
      }).then(function (result) {
        if (result.value) {
          _this.$store.dispatch('addEq', params);

          _this.equipamiento.equipo = '';
          _this.equipamiento.precio = '';
          _this.equipamiento.tipo = '';
          _this.equipamiento.unidad = '';
          _this.equipamiento.cantidad = '';
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          _this.equipamiento.equipo = '';
          _this.equipamiento.precio = '';
          _this.equipamiento.tipo = '';
          _this.equipamiento.unidad = '';
          _this.equipamiento.cantidad = '';
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/EquipoNuevo.vue?vue&type=template&id=c0209ac6&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/EquipoNuevo.vue?vue&type=template&id=c0209ac6& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "columns is-mobile is-centered is-vcentered",
      attrs: { id: "" }
    },
    [
      _c("article", { staticClass: "column is-6-desktop is-8-tablet" }, [
        _c(
          "form",
          {
            attrs: { method: "post" },
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.nuevoEquipo($event)
              }
            }
          },
          [
            _c("h1", { staticClass: "is-size-1" }, [
              _vm._v("\n                Nuevo Equipo\n            ")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c("label", { staticClass: "label", attrs: { for: "equipo" } }, [
                _vm._v("\n                    Equipo\n                ")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.equipamiento.equipo,
                    expression: "equipamiento.equipo"
                  }
                ],
                staticClass: "input",
                attrs: {
                  type: "text",
                  name: "equipo",
                  placeholder: "Nombre del equipo"
                },
                domProps: { value: _vm.equipamiento.equipo },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.equipamiento, "equipo", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c("label", { staticClass: "label", attrs: { for: "precio" } }, [
                _vm._v("\n                    Precio\n                ")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.equipamiento.precio,
                    expression: "equipamiento.precio",
                    modifiers: { number: true }
                  }
                ],
                staticClass: "input",
                attrs: {
                  type: "text",
                  name: "precio",
                  placeholder: "Precio del equipo"
                },
                domProps: { value: _vm.equipamiento.precio },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.equipamiento,
                      "precio",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function($event) {
                    _vm.$forceUpdate()
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c("div", { staticClass: "control" }, [
                _c("label", { staticClass: "label", attrs: { for: "tipo" } }, [
                  _vm._v("\n                        Tipo\n                    ")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "select" }, [
                  _c(
                    "select",
                    {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.equipamiento.tipo,
                          expression: "equipamiento.tipo"
                        }
                      ],
                      attrs: { name: "tipo" },
                      on: {
                        change: function($event) {
                          var $$selectedVal = Array.prototype.filter
                            .call($event.target.options, function(o) {
                              return o.selected
                            })
                            .map(function(o) {
                              var val = "_value" in o ? o._value : o.value
                              return val
                            })
                          _vm.$set(
                            _vm.equipamiento,
                            "tipo",
                            $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          )
                        }
                      }
                    },
                    [
                      _c("option", { attrs: { value: "Tipo 1" } }, [
                        _vm._v(
                          "\n                                Tipo 1\n                            "
                        )
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Tipo 2" } }, [
                        _vm._v(
                          "\n                                Tipo 2\n                            "
                        )
                      ]),
                      _vm._v(" "),
                      _c("option", { attrs: { value: "Tipo 3" } }, [
                        _vm._v(
                          "\n                                Tipo 3\n                            "
                        )
                      ])
                    ]
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c("label", { staticClass: "label", attrs: { for: "unidad" } }, [
                _vm._v("\n                    Unidad\n                ")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.equipamiento.unidad,
                    expression: "equipamiento.unidad"
                  }
                ],
                staticClass: "input",
                attrs: { type: "text", name: "unidad", value: "" },
                domProps: { value: _vm.equipamiento.unidad },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.equipamiento, "unidad", $event.target.value)
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c(
                "label",
                { staticClass: "label", attrs: { for: "cantidad" } },
                [_vm._v("\n                    Cantidad\n                ")]
              ),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model.number",
                    value: _vm.equipamiento.cantidad,
                    expression: "equipamiento.cantidad",
                    modifiers: { number: true }
                  }
                ],
                staticClass: "input",
                attrs: { type: "text", name: "cantidad", value: "" },
                domProps: { value: _vm.equipamiento.cantidad },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.equipamiento,
                      "cantidad",
                      _vm._n($event.target.value)
                    )
                  },
                  blur: function($event) {
                    _vm.$forceUpdate()
                  }
                }
              })
            ]),
            _vm._v(" "),
            _vm._m(0)
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "field" }, [
      _c("div", { staticClass: "buttons" }, [
        _c(
          "button",
          {
            staticClass: "button is-success",
            attrs: { type: "submit", name: "button" }
          },
          [_vm._v("\n                        Añadir\n                    ")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/EquipoNuevo.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/EquipoNuevo.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EquipoNuevo_vue_vue_type_template_id_c0209ac6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EquipoNuevo.vue?vue&type=template&id=c0209ac6& */ "./resources/js/components/EquipoNuevo.vue?vue&type=template&id=c0209ac6&");
/* harmony import */ var _EquipoNuevo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EquipoNuevo.vue?vue&type=script&lang=js& */ "./resources/js/components/EquipoNuevo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EquipoNuevo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EquipoNuevo_vue_vue_type_template_id_c0209ac6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EquipoNuevo_vue_vue_type_template_id_c0209ac6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/EquipoNuevo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/EquipoNuevo.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/EquipoNuevo.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoNuevo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./EquipoNuevo.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/EquipoNuevo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoNuevo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/EquipoNuevo.vue?vue&type=template&id=c0209ac6&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/EquipoNuevo.vue?vue&type=template&id=c0209ac6& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoNuevo_vue_vue_type_template_id_c0209ac6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./EquipoNuevo.vue?vue&type=template&id=c0209ac6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/EquipoNuevo.vue?vue&type=template&id=c0209ac6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoNuevo_vue_vue_type_template_id_c0209ac6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoNuevo_vue_vue_type_template_id_c0209ac6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);