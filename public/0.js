(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClienteCard.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ClienteCard.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ClienteCard',
  props: {
    data: {
      default: function _default() {},
      type: Object
    }
  },
  data: function data() {
    return {
      cliente: _objectSpread({}, this.data)
    };
  },
  created: function created() {},
  methods: {
    borrar: function borrar(id, cliente) {
      var _this = this;

      Swal.fire({
        title: '¿Estás seguro de quitar a ' + cliente + ' de la lista?',
        type: 'warning',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, estoy seguro',
        cancelButtonText: 'Cancelar'
      }).then(function (result) {
        if (result.value) {
          var url = '/clientes/' + id + '/delete';
          axios__WEBPACK_IMPORTED_MODULE_0___default.a.delete(url).then(function (response) {
            _this.$emit('actualizar');

            Swal.fire('Listo', 'El cliente ha sido quitado de la lista', 'success');
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClienteNuevo.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ClienteNuevo.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* global Swal: true */
 //NuevoCliente.equipamiento provicional

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ClienteNuevo',
  data: function data() {
    return {
      planes: [],
      equipos: [],
      nuevoCliente: {
        nombre: '',
        apellido: '',
        direccion: '',
        email: '',
        telefono: '',
        planID: '',
        equipamiento: [],
        cuotas: ''
      },
      extra: false,
      tasa: 11
    };
  },
  created: function created() {
    this.selectPlanes();
    this.selectEquipos();
  },
  methods: {
    selectPlanes: function selectPlanes() {
      var _this = this;

      var url = '/planes/get';
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url).then(function (response) {
        _this.planes = response.data;
      });
    },
    selectEquipos: function selectEquipos() {
      var _this2 = this;

      var url = '/equipos/get';
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url).then(function (response) {
        _this2.equipos = response.data;

        _this2.objEquipamiento(_this2.equipos);
      });
    },
    objEquipamiento: function objEquipamiento(equipamiento) {
      var _this3 = this;

      equipamiento.forEach(function (eq) {
        var nuevo = {
          id: '',
          cantidad: ''
        };

        _this3.nuevoCliente.equipamiento.push(nuevo);
      });
    },
    createCliente: function createCliente() {
      var _this4 = this;

      var params = Object.assign({}, this.nuevoCliente);
      var url = '/clientes/store';
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(url, params).then(function (response) {
        _this4.nuevoCliente.nombre = '';
        _this4.nuevoCliente.apellido = '';
        _this4.nuevoCliente.planID = '';
        _this4.nuevoCliente.direccion = '';
        _this4.nuevoCliente.telefono = '';
        _this4.nuevoCliente.email = '';

        _this4.objEquipamiento(_this4.equipos);

        Swal.fire('Listo', 'Cliente registrado exitósamente', 'success');
      });
    },
    calculo: function calculo() {//
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Clientes.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Clientes.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ClientesIndex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ClientesIndex */ "./resources/js/components/ClientesIndex.vue");
/* harmony import */ var _ClienteNuevo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ClienteNuevo */ "./resources/js/components/ClienteNuevo.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Clientes',
  components: {
    ClientesIndex: _ClientesIndex__WEBPACK_IMPORTED_MODULE_1__["default"],
    ClienteNuevo: _ClienteNuevo__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      selectedTab: {
        index: false,
        nuevo: false
      },
      clientes: []
    };
  },
  created: function created() {
    this.index();
  },
  methods: {
    index: function index() {
      var _this = this;

      var url = '/clientes/get';
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url).then(function (response) {
        _this.clientes = response.data;
      });
    },
    tab: function tab(_tab) {
      var section = this.selectedTab;
      var active = this.activeTab;

      switch (_tab) {
        case 'index':
          section.index = true;
          section.nuevo = false;
          break;

        case 'nuevo':
          section.index = false;
          section.nuevo = true;
          break;

        default:
          break;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClientesIndex.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ClientesIndex.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ClienteCard_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ClienteCard.vue */ "./resources/js/components/ClienteCard.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'ClientesIndex',
  components: {
    ClienteCard: _ClienteCard_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    data: {
      default: function _default() {
        [];
      },
      type: Array
    }
  },
  data: function data() {
    return {
      clientes: _toConsumableArray(this.data),
      paginate: ['clientes']
    };
  },
  methods: {
    actualizar: function actualizar() {
      this.$emit('actualizar');
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClienteCard.vue?vue&type=template&id=027a83e2&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ClienteCard.vue?vue&type=template&id=027a83e2& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card" }, [
    _c("div", { staticClass: "card-header" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("h3", { staticClass: "card-header-title is-size-3" }, [
        _vm._v(
          "\n      " +
            _vm._s(_vm.cliente.nombre) +
            " " +
            _vm._s(_vm.cliente.apellido) +
            "\n    "
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "card-content" }, [
      _c("div", { staticClass: "card-body" }, [
        _c("table", { staticClass: "table is-narrow is-fullwidth" }, [
          _c("tbody", [
            _c("tr", [
              _c("td", [_vm._v("Plan")]),
              _vm._v(" "),
              _c("td", {
                domProps: { textContent: _vm._s(_vm.cliente.plan.velocidad) }
              })
            ]),
            _vm._v(" "),
            _c("tr", [
              _c("td", [_vm._v("Teléfono")]),
              _vm._v(" "),
              _c("td", {
                domProps: { textContent: _vm._s(_vm.cliente.telefono) }
              })
            ]),
            _vm._v(" "),
            _vm.cliente.email
              ? _c("tr", [
                  _c("td", [_vm._v("E-mail")]),
                  _vm._v(" "),
                  _c("td", {
                    domProps: { textContent: _vm._s(_vm.cliente.email) }
                  })
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("tr", [
              _c("td", [_vm._v("Dirección")]),
              _vm._v(" "),
              _c("td", {
                domProps: { textContent: _vm._s(_vm.cliente.direccion) }
              })
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "card-footer" }, [
      _vm._m(1),
      _vm._v(" "),
      _vm._m(2),
      _vm._v(" "),
      _vm._m(3),
      _vm._v(" "),
      _c("div", { staticClass: "card-footer-item" }, [
        _c(
          "button",
          {
            staticClass: "button is-danger tooltip",
            attrs: { "data-tooltip": "Dar de baja" },
            on: {
              click: function($event) {
                $event.preventDefault()
                _vm.borrar(_vm.cliente.id, _vm.cliente.nombre)
              }
            }
          },
          [_vm._m(4)]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "card-header-icon" }, [
      _c("i", { staticClass: "fas fa-user" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-footer-item" }, [
      _c(
        "button",
        {
          staticClass: "button is-link tooltip",
          attrs: { "data-tooltip": "Control de pago" }
        },
        [
          _c("span", { staticClass: "icon is-small" }, [
            _c("i", { staticClass: "fas fa-file-invoice" })
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-footer-item" }, [
      _c(
        "button",
        {
          staticClass: "button is-dark tooltip",
          attrs: { "data-tooltip": "Editar información" }
        },
        [
          _c("span", { staticClass: "icon is-small" }, [
            _c("i", { staticClass: "fas fa-edit" })
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-footer-item" }, [
      _c(
        "button",
        {
          staticClass: "button is-warning tooltip",
          attrs: { "data-tooltip": "Pausar contrato" }
        },
        [
          _c("span", { staticClass: "icon is-small" }, [
            _c("i", { staticClass: "fas fa-user-clock" })
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "icon is-small" }, [
      _c("i", { staticClass: "fas fa-user-minus" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClienteNuevo.vue?vue&type=template&id=6ed27370&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ClienteNuevo.vue?vue&type=template&id=6ed27370& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "columns is-mobile is-centered is-vcentered",
      attrs: { id: "cliente-nuevo" }
    },
    [
      _c("article", { staticClass: "column is-6-desktop is-8-tablet" }, [
        _c(
          "form",
          {
            attrs: { method: "post" },
            on: {
              submit: function($event) {
                $event.preventDefault()
                return _vm.createCliente($event)
              }
            }
          },
          [
            _c("h1", { staticClass: "is-size-1" }, [
              _vm._v("\n                Registro de clientes\n            ")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c("label", { staticClass: "label", attrs: { for: "nombre" } }, [
                _vm._v("\n                    Nombre\n                ")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "control" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nuevoCliente.nombre,
                      expression: "nuevoCliente.nombre"
                    }
                  ],
                  staticClass: "input",
                  attrs: {
                    type: "text",
                    name: "nombre",
                    placeholder: "Nombre/s",
                    required: ""
                  },
                  domProps: { value: _vm.nuevoCliente.nombre },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.nuevoCliente, "nombre", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c(
                "label",
                { staticClass: "label", attrs: { for: "apellido" } },
                [_vm._v("\n                    Apellido\n                ")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "control" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nuevoCliente.apellido,
                      expression: "nuevoCliente.apellido"
                    }
                  ],
                  staticClass: "input",
                  attrs: {
                    type: "text",
                    name: "apellido",
                    placeholder: "Apellido/s",
                    required: ""
                  },
                  domProps: { value: _vm.nuevoCliente.apellido },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.nuevoCliente,
                        "apellido",
                        $event.target.value
                      )
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field", attrs: { id: "plan-cliente" } }, [
              _c(
                "label",
                { staticClass: "label", attrs: { for: "plan-cliente" } },
                [
                  _vm._v(
                    "\n                    Plan a suscribirse\n                "
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "control" },
                _vm._l(_vm.planes, function(plan) {
                  return _c(
                    "label",
                    { key: plan.id, attrs: { for: plan.id } },
                    [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.number",
                            value: _vm.nuevoCliente.planID,
                            expression: "nuevoCliente.planID",
                            modifiers: { number: true }
                          }
                        ],
                        attrs: { id: plan.id, type: "radio", name: "plan_id" },
                        domProps: {
                          value: plan.id,
                          checked: _vm._q(
                            _vm.nuevoCliente.planID,
                            _vm._n(plan.id)
                          )
                        },
                        on: {
                          change: function($event) {
                            _vm.$set(
                              _vm.nuevoCliente,
                              "planID",
                              _vm._n(plan.id)
                            )
                          }
                        }
                      }),
                      _vm._v(
                        "\n                        " +
                          _vm._s(plan.velocidad) +
                          "\n                    "
                      )
                    ]
                  )
                }),
                0
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c(
                "label",
                { staticClass: "label", attrs: { for: "direccion" } },
                [_vm._v("\n                    Dirección\n                ")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "control" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nuevoCliente.direccion,
                      expression: "nuevoCliente.direccion"
                    }
                  ],
                  staticClass: "input",
                  attrs: {
                    type: "text",
                    name: "direccion",
                    placeholder: "Dirección",
                    required: ""
                  },
                  domProps: { value: _vm.nuevoCliente.direccion },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.nuevoCliente,
                        "direccion",
                        $event.target.value
                      )
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c(
                "label",
                { staticClass: "label", attrs: { for: "telefono" } },
                [_vm._v("\n                    Teléfono\n                ")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "control" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nuevoCliente.telefono,
                      expression: "nuevoCliente.telefono"
                    }
                  ],
                  staticClass: "input",
                  attrs: {
                    type: "text",
                    name: "telefono",
                    placeholder: "Teléfono",
                    required: ""
                  },
                  domProps: { value: _vm.nuevoCliente.telefono },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(
                        _vm.nuevoCliente,
                        "telefono",
                        $event.target.value
                      )
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "field" }, [
              _c("label", { staticClass: "label", attrs: { for: "email" } }, [
                _vm._v("\n                    E-mail\n                ")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "control" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.nuevoCliente.email,
                      expression: "nuevoCliente.email"
                    }
                  ],
                  staticClass: "input",
                  attrs: {
                    type: "email",
                    name: "email",
                    placeholder: "E-mail"
                  },
                  domProps: { value: _vm.nuevoCliente.email },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.nuevoCliente, "email", $event.target.value)
                    }
                  }
                })
              ])
            ]),
            _vm._v(" "),
            _c(
              "transition",
              {
                attrs: {
                  mode: "out-in",
                  "enter-active-class": "animated fadeIn",
                  "leave-active-class": "animated fadeOut"
                }
              },
              [
                _vm.extra
                  ? _c("div", { key: "extra", staticClass: "field" }, [
                      _c("div", { staticClass: "card" }, [
                        _c("div", { staticClass: "card-header" }, [
                          _c("div", { staticClass: "card-header-title" }, [
                            _vm._v(
                              "\n                                Bonificaciones o cargos extra\n                            "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              staticClass: "card-header-icon has-text-dark",
                              attrs: { "aria-label": "cerrar" },
                              on: {
                                click: function($event) {
                                  $event.preventDefault()
                                  _vm.extra = !_vm.extra
                                }
                              }
                            },
                            [
                              _c("span", { staticClass: "icon" }, [
                                _c("i", { staticClass: "fas fa-angle-up" })
                              ])
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "card-content" }, [
                          _c(
                            "div",
                            { staticClass: "card-body" },
                            [
                              _c("h5", { staticClass: "is-size-5" }, [
                                _vm._v(
                                  "\n                                    Equipamiento\n                                "
                                )
                              ]),
                              _vm._v(" "),
                              _c("p", { staticClass: "help has-text-info" }, [
                                _vm._v(
                                  "\n                                    mensaje para orientar\n                                "
                                )
                              ]),
                              _vm._v(" "),
                              _vm._l(_vm.equipos, function(equipo, i) {
                                return _c(
                                  "div",
                                  { key: equipo.id, staticClass: "control" },
                                  [
                                    _c("label", { attrs: { for: equipo.id } }, [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model.number",
                                            value:
                                              _vm.nuevoCliente.equipamiento[i]
                                                .id,
                                            expression:
                                              "\n                                                nuevoCliente.equipamiento[i]\n                                                    .id\n                                            ",
                                            modifiers: { number: true }
                                          }
                                        ],
                                        attrs: {
                                          id: equipo.id,
                                          "true-value": equipo.id,
                                          "false-value": "0",
                                          type: "checkbox"
                                        },
                                        domProps: {
                                          checked: Array.isArray(
                                            _vm.nuevoCliente.equipamiento[i].id
                                          )
                                            ? _vm._i(
                                                _vm.nuevoCliente.equipamiento[i]
                                                  .id,
                                                null
                                              ) > -1
                                            : _vm._q(
                                                _vm.nuevoCliente.equipamiento[i]
                                                  .id,
                                                equipo.id
                                              )
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a =
                                                _vm.nuevoCliente.equipamiento[i]
                                                  .id,
                                              $$el = $event.target,
                                              $$c = $$el.checked
                                                ? equipo.id
                                                : "0"
                                            if (Array.isArray($$a)) {
                                              var $$v = _vm._n(null),
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    _vm.nuevoCliente
                                                      .equipamiento[i],
                                                    "id",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    _vm.nuevoCliente
                                                      .equipamiento[i],
                                                    "id",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(
                                                _vm.nuevoCliente.equipamiento[
                                                  i
                                                ],
                                                "id",
                                                $$c
                                              )
                                            }
                                          }
                                        }
                                      }),
                                      _vm._v(
                                        "\n                                        " +
                                          _vm._s(equipo.equipo) +
                                          "\n                                    "
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model.number",
                                          value:
                                            _vm.nuevoCliente.equipamiento[i]
                                              .cantidad,
                                          expression:
                                            "\n                                            nuevoCliente.equipamiento[i]\n                                                .cantidad\n                                        ",
                                          modifiers: { number: true }
                                        }
                                      ],
                                      staticClass: "input",
                                      attrs: {
                                        type: "number",
                                        name: i,
                                        placeholder: "Cantidad"
                                      },
                                      domProps: {
                                        value:
                                          _vm.nuevoCliente.equipamiento[i]
                                            .cantidad
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.nuevoCliente.equipamiento[i],
                                            "cantidad",
                                            _vm._n($event.target.value)
                                          )
                                        },
                                        blur: function($event) {
                                          _vm.$forceUpdate()
                                        }
                                      }
                                    })
                                  ]
                                )
                              })
                            ],
                            2
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "card-body" }, [
                            _c("h5", { staticClass: "is-size-5" }, [
                              _vm._v(
                                "\n                                    Cuotas\n                                "
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "control" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model.number",
                                    value: _vm.nuevoCliente.cuotas,
                                    expression: "nuevoCliente.cuotas",
                                    modifiers: { number: true }
                                  }
                                ],
                                staticClass: "input",
                                attrs: {
                                  type: "number",
                                  name: "cuotas",
                                  placeholder: "Cantidad de cuotas"
                                },
                                domProps: { value: _vm.nuevoCliente.cuotas },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.nuevoCliente,
                                      "cuotas",
                                      _vm._n($event.target.value)
                                    )
                                  },
                                  blur: function($event) {
                                    _vm.$forceUpdate()
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "card-body" }, [
                            _c("div", { staticClass: "control" }, [
                              _c("p", { staticClass: "help has-text-info" }, [
                                _vm._v(
                                  "\n                                        Calculo de cuotas\n                                    "
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "button",
                                {
                                  staticClass:
                                    "button is-small is-outlined is-info",
                                  attrs: { type: "button", name: "button" }
                                },
                                [
                                  _vm._v(
                                    "\n                                        Calculo\n                                    "
                                  )
                                ]
                              )
                            ])
                          ])
                        ])
                      ])
                    ])
                  : _c("div", { key: "button", staticClass: "field" }, [
                      _c(
                        "button",
                        {
                          staticClass: "button is-link is-inverted",
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              _vm.extra = !_vm.extra
                            }
                          }
                        },
                        [
                          _c("span", [_vm._v("Extra")]),
                          _vm._v(" "),
                          _c("span", { staticClass: "icon is-small" }, [
                            _c("i", { staticClass: "fas fa-angle-down" })
                          ])
                        ]
                      )
                    ])
              ]
            ),
            _vm._v(" "),
            _vm._m(0)
          ],
          1
        )
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "field" }, [
      _c("div", { staticClass: "buttons" }, [
        _c(
          "button",
          {
            staticClass: "button is-success",
            attrs: { type: "submit", name: "button" }
          },
          [_vm._v("\n                        Registrar\n                    ")]
        ),
        _vm._v(" "),
        _c(
          "button",
          { staticClass: "button", attrs: { type: "button", name: "button" } },
          [_vm._v("\n                        Cancelar\n                    ")]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Clientes.vue?vue&type=template&id=39326ab8&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Clientes.vue?vue&type=template&id=39326ab8& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "clientes" } },
    [
      _c("div", { staticClass: "tabs is-centered" }, [
        _c("ul", [
          _c(
            "li",
            { key: "tab-index", class: { "is-active": _vm.selectedTab.index } },
            [
              _c(
                "a",
                {
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.tab("index")
                    }
                  }
                },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("span", [_vm._v("Todos los clientes")])
                ]
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "li",
            { key: "tab-nuevo", class: { "is-active": _vm.selectedTab.nuevo } },
            [
              _c(
                "a",
                {
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.tab("nuevo")
                    }
                  }
                },
                [_vm._m(1), _vm._v(" "), _c("span", [_vm._v("Nuevo Cliente")])]
              )
            ]
          ),
          _vm._v(" "),
          _vm._m(2)
        ])
      ]),
      _vm._v(" "),
      _c(
        "transition",
        {
          attrs: {
            "enter-active-class": "animated fadeInLeft",
            "leave-active-class": "animated fadeOutRight",
            mode: "out-in"
          }
        },
        [
          _vm.selectedTab.index
            ? _c(
                "section",
                { key: "index", staticClass: "section" },
                [
                  _c("ClientesIndex", {
                    attrs: { data: _vm.clientes },
                    on: { actualizar: _vm.index }
                  })
                ],
                1
              )
            : _vm.selectedTab.nuevo
            ? _c(
                "section",
                { key: "nuevo", staticClass: "section" },
                [_c("ClienteNuevo")],
                1
              )
            : _c("section", { staticClass: "section" }, [
                _c("div", { staticClass: "is-size-1 has-text-centered" }, [
                  _vm._v("\n        Clientes\n      ")
                ])
              ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "icon is-small" }, [
      _c("i", { staticClass: "fas fa-users", attrs: { "aria-hidden": "true" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "icon is-small" }, [
      _c("i", {
        staticClass: "fas fa-user-plus",
        attrs: { "aria-hidden": "true" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("a", { attrs: { href: "#" } }, [
        _c("span", { staticClass: "icon is-small" }, [
          _c("i", {
            staticClass: "fas fa-user-clock",
            attrs: { "aria-hidden": "true" }
          })
        ]),
        _vm._v(" "),
        _c("span", [_vm._v("Clientes inactivos")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClientesIndex.vue?vue&type=template&id=028f5c84&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ClientesIndex.vue?vue&type=template&id=028f5c84& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "columns is-multiline", attrs: { id: "clientes-index" } },
    [
      _c("div", { staticClass: "column is-full" }, [
        _c(
          "article",
          [
            _c(
              "paginate",
              {
                attrs: {
                  list: _vm.clientes,
                  per: 4,
                  name: "clientes",
                  tag: "div"
                }
              },
              [
                _c(
                  "transition-group",
                  {
                    staticClass: "columns is-multiline",
                    attrs: {
                      tag: "div",
                      "enter-active-class": "animated fadeInLeft",
                      "leave-active-class": "animated fadeOutRight"
                    }
                  },
                  _vm._l(_vm.paginated("clientes"), function(cliente) {
                    return _c(
                      "div",
                      {
                        key: cliente.id,
                        staticClass:
                          "column is-one-quarter-desktop is-full-mobile is-5-tablet"
                      },
                      [
                        _c("ClienteCard", {
                          attrs: { data: cliente },
                          on: { actualizar: _vm.actualizar }
                        })
                      ],
                      1
                    )
                  }),
                  0
                )
              ],
              1
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "column is-full is-centered" }, [
        _c(
          "nav",
          {
            staticClass: "pagination",
            attrs: { role: "pagination", "aria-label": "pagination" }
          },
          [
            _c("paginate-links", {
              attrs: {
                for: "clientes",
                simple: {
                  prev: "Anterior",
                  next: "Siguiente"
                },
                "hide-single-page": true,
                classes: {
                  ul: "pagination-list",
                  "ul > li.prev > a": "pagination-previous",
                  "ul > li.next > a": "pagination-next"
                }
              }
            })
          ],
          1
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/ClienteCard.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/ClienteCard.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ClienteCard_vue_vue_type_template_id_027a83e2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ClienteCard.vue?vue&type=template&id=027a83e2& */ "./resources/js/components/ClienteCard.vue?vue&type=template&id=027a83e2&");
/* harmony import */ var _ClienteCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ClienteCard.vue?vue&type=script&lang=js& */ "./resources/js/components/ClienteCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ClienteCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ClienteCard_vue_vue_type_template_id_027a83e2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ClienteCard_vue_vue_type_template_id_027a83e2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/ClienteCard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/ClienteCard.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/ClienteCard.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ClienteCard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClienteCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/ClienteCard.vue?vue&type=template&id=027a83e2&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/ClienteCard.vue?vue&type=template&id=027a83e2& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteCard_vue_vue_type_template_id_027a83e2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ClienteCard.vue?vue&type=template&id=027a83e2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClienteCard.vue?vue&type=template&id=027a83e2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteCard_vue_vue_type_template_id_027a83e2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteCard_vue_vue_type_template_id_027a83e2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/ClienteNuevo.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/ClienteNuevo.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ClienteNuevo_vue_vue_type_template_id_6ed27370___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ClienteNuevo.vue?vue&type=template&id=6ed27370& */ "./resources/js/components/ClienteNuevo.vue?vue&type=template&id=6ed27370&");
/* harmony import */ var _ClienteNuevo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ClienteNuevo.vue?vue&type=script&lang=js& */ "./resources/js/components/ClienteNuevo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ClienteNuevo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ClienteNuevo_vue_vue_type_template_id_6ed27370___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ClienteNuevo_vue_vue_type_template_id_6ed27370___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/ClienteNuevo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/ClienteNuevo.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/ClienteNuevo.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteNuevo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ClienteNuevo.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClienteNuevo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteNuevo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/ClienteNuevo.vue?vue&type=template&id=6ed27370&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/ClienteNuevo.vue?vue&type=template&id=6ed27370& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteNuevo_vue_vue_type_template_id_6ed27370___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ClienteNuevo.vue?vue&type=template&id=6ed27370& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClienteNuevo.vue?vue&type=template&id=6ed27370&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteNuevo_vue_vue_type_template_id_6ed27370___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClienteNuevo_vue_vue_type_template_id_6ed27370___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/Clientes.vue":
/*!**********************************************!*\
  !*** ./resources/js/components/Clientes.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Clientes_vue_vue_type_template_id_39326ab8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Clientes.vue?vue&type=template&id=39326ab8& */ "./resources/js/components/Clientes.vue?vue&type=template&id=39326ab8&");
/* harmony import */ var _Clientes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Clientes.vue?vue&type=script&lang=js& */ "./resources/js/components/Clientes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Clientes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Clientes_vue_vue_type_template_id_39326ab8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Clientes_vue_vue_type_template_id_39326ab8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Clientes.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Clientes.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/components/Clientes.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Clientes.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Clientes.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Clientes.vue?vue&type=template&id=39326ab8&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/Clientes.vue?vue&type=template&id=39326ab8& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_template_id_39326ab8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Clientes.vue?vue&type=template&id=39326ab8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Clientes.vue?vue&type=template&id=39326ab8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_template_id_39326ab8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Clientes_vue_vue_type_template_id_39326ab8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/ClientesIndex.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/ClientesIndex.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ClientesIndex_vue_vue_type_template_id_028f5c84___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ClientesIndex.vue?vue&type=template&id=028f5c84& */ "./resources/js/components/ClientesIndex.vue?vue&type=template&id=028f5c84&");
/* harmony import */ var _ClientesIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ClientesIndex.vue?vue&type=script&lang=js& */ "./resources/js/components/ClientesIndex.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ClientesIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ClientesIndex_vue_vue_type_template_id_028f5c84___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ClientesIndex_vue_vue_type_template_id_028f5c84___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/ClientesIndex.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/ClientesIndex.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/ClientesIndex.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ClientesIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ClientesIndex.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClientesIndex.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ClientesIndex_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/ClientesIndex.vue?vue&type=template&id=028f5c84&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/ClientesIndex.vue?vue&type=template&id=028f5c84& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClientesIndex_vue_vue_type_template_id_028f5c84___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ClientesIndex.vue?vue&type=template&id=028f5c84& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ClientesIndex.vue?vue&type=template&id=028f5c84&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClientesIndex_vue_vue_type_template_id_028f5c84___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClientesIndex_vue_vue_type_template_id_028f5c84___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);