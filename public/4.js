(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/EquipoEdit.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/EquipoEdit.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'EquipoEdit',
  props: {
    id: {
      type: Integer,
      default: null
    }
  },
  computed: {
    equipo: function equipo() {
      return this.$store.getters.singleEq(this.id);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/EquipoEdit.vue?vue&type=template&id=131b5914&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/EquipoEdit.vue?vue&type=template&id=131b5914& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card" }, [
    _c("form", [
      _c("div", { staticClass: "card-header" }, [
        _c("h3", { staticClass: "card-header-title is-size-3" }, [
          _c("div", { staticClass: "field" }, [
            _c("div", { staticClass: "control" }, [
              _c("label", { staticClass: "label", attrs: { for: "equipo" } }, [
                _vm._v("Nombre del equipo")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.equipo.equipo,
                    expression: "equipo.equipo"
                  }
                ],
                staticClass: "input",
                attrs: {
                  type: "text",
                  name: "equipo",
                  placeholder: "Nombre del equipo"
                },
                domProps: { value: _vm.equipo.equipo },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.equipo, "equipo", $event.target.value)
                  }
                }
              })
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "card-content" }, [
        _c("div", { staticClass: "card-body" }, [
          _c("table", { staticClass: "table is-narrow is-fullwidth" }, [
            _c("tbody", [
              _c("tr", [
                _c("td", [_vm._v("Precio")]),
                _vm._v(" "),
                _c("td", [
                  _c("div", { staticClass: "field" }, [
                    _c("div", { staticClass: "control" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.number",
                            value: _vm.equipo.precio,
                            expression: "equipo.precio",
                            modifiers: { number: true }
                          }
                        ],
                        staticClass: "input",
                        attrs: {
                          type: "number",
                          name: "precio",
                          placeholder: "Precio"
                        },
                        domProps: { value: _vm.equipo.precio },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.equipo,
                              "precio",
                              _vm._n($event.target.value)
                            )
                          },
                          blur: function($event) {
                            _vm.$forceUpdate()
                          }
                        }
                      })
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("tr", [
                _c("td", [_vm._v("Cantidad")]),
                _vm._v(" "),
                _c("td", [
                  _c("div", { staticClass: "field" }, [
                    _c("div", { staticClass: "control" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.number",
                            value: _vm.equipo.cantidad,
                            expression: "equipo.cantidad",
                            modifiers: { number: true }
                          }
                        ],
                        staticClass: "input",
                        attrs: {
                          name: "cantidad",
                          placeholder: "Stock",
                          type: "number"
                        },
                        domProps: { value: _vm.equipo.cantidad },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.equipo,
                              "cantidad",
                              _vm._n($event.target.value)
                            )
                          },
                          blur: function($event) {
                            _vm.$forceUpdate()
                          }
                        }
                      })
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("tr", [
                _c("td", [_vm._v("Unidad")]),
                _vm._v(" "),
                _c("td", [
                  _c("div", { staticClass: "field" }, [
                    _c("div", { staticClass: "control" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.equipo.unidad,
                            expression: "equipo.unidad"
                          }
                        ],
                        staticClass: "input",
                        attrs: {
                          type: "text",
                          name: "unidad",
                          placeholder: "Unidad de medida"
                        },
                        domProps: { value: _vm.equipo.unidad },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.equipo, "unidad", $event.target.value)
                          }
                        }
                      })
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("tr", [
                _c("td", [
                  _vm._v(
                    "\n                                Tipo\n                            "
                  )
                ]),
                _vm._v(" "),
                _c("td", [
                  _c("div", { staticClass: "field" }, [
                    _c("div", { staticClass: "control" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.equipo.tipo,
                            expression: "equipo.tipo"
                          }
                        ],
                        staticClass: "input",
                        attrs: {
                          type: "text",
                          name: "input",
                          placeholder: "Tipo de equipamiento"
                        },
                        domProps: { value: _vm.equipo.tipo },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.equipo, "tipo", $event.target.value)
                          }
                        }
                      })
                    ])
                  ])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._m(0)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-footer" }, [
      _c("div", { staticClass: "card-footer-item" }, [
        _c("button", { staticClass: "button is-success is-inverted" }, [
          _c("span", { staticClass: "icon is-small" }, [
            _c("i", { staticClass: "fas fa-check" })
          ]),
          _vm._v(" "),
          _c("span", [_vm._v("Actualizar")])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "card-footer-item" }, [
        _c("button", { staticClass: "button is-danger is-inverted" }, [
          _c("span", { staticClass: "icon is-small" }, [
            _c("i", { staticClass: "far fa-eye-slash" })
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/EquipoEdit.vue":
/*!************************************************!*\
  !*** ./resources/js/components/EquipoEdit.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EquipoEdit_vue_vue_type_template_id_131b5914___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EquipoEdit.vue?vue&type=template&id=131b5914& */ "./resources/js/components/EquipoEdit.vue?vue&type=template&id=131b5914&");
/* harmony import */ var _EquipoEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EquipoEdit.vue?vue&type=script&lang=js& */ "./resources/js/components/EquipoEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EquipoEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EquipoEdit_vue_vue_type_template_id_131b5914___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EquipoEdit_vue_vue_type_template_id_131b5914___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/EquipoEdit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/EquipoEdit.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/EquipoEdit.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./EquipoEdit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/EquipoEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/EquipoEdit.vue?vue&type=template&id=131b5914&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/EquipoEdit.vue?vue&type=template&id=131b5914& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoEdit_vue_vue_type_template_id_131b5914___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./EquipoEdit.vue?vue&type=template&id=131b5914& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/EquipoEdit.vue?vue&type=template&id=131b5914&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoEdit_vue_vue_type_template_id_131b5914___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EquipoEdit_vue_vue_type_template_id_131b5914___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);