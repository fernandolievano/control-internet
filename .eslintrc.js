module.exports = {
    "root": true,
    "env": {
        "node": true
    },
    "extends": [
        "plugin:vue/recommended",
        // "eslint:recommended",
        "plugin:prettier/recommended",
        "prettier/vue"
    ],
    "parserOptions": {
        "parser": "babel-eslint"
    }
}
