<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/registrame', 'ClienteController@admin');

Route::prefix('/clientes')->name('clientes.')->group(function () {
    Route::get('/', 'ClienteController@index')->name('index');
    Route::get('/get', 'ClienteController@clientes')->name('get');
    Route::post('/store', 'ClienteController@store')->name('store');
    Route::delete('/{id}/delete', 'ClienteController@destroy')->name('delete');
});

Route::prefix('/equipamiento')->name('equipos.')->group(function () {
    Route::get('/', 'EquipamientoController@index')->name('index');
    Route::get('/get', 'EquipamientoController@equipos')->name('get');
    Route::put('/{id}/update', 'EquipamientoController@update')->name('update');
    Route::post('/store', 'EquipamientoController@store')->name('store');
    Route::delete('/{id}/delete', 'EquipamientoController@destroy')->name('delete');
});

Route::prefix('/planes')->name('planes.')->group(function () {
    Route::get('/', 'PlanController@index')->name('index');
    Route::get('/get', 'PlanController@planes')->name('get');
    Route::post('/store', 'PlanController@store')->name('store');
    Route::delete('/{id}/delete', 'PlanController@destroy')->name('delete');
});

Route::get('/home', 'HomeController@index')->name('home');
