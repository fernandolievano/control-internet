import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex, axios)

export default new Vuex.Store({
    state: {
        planes: [],
        equipamiento: []
    },
    actions: {
        getPlanes({ commit }) {
            axios
                .get('/planes/get')
                .then(response => {
                    let planes = response.data
                    commit('SET_PLANES', planes)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        getEq({ commit }) {
            axios
                .get('/equipamiento/get')
                .then(response => {
                    let equipamiento = response.data

                    for (let i = 0; i < equipamiento.length; i++) {
                        var element = equipamiento[i]
                        Object.assign(element, { edit: false })
                    }

                    commit('SET_EQ', equipamiento)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        addEq({ commit }, equipamiento) {
            axios.post('/equipamiento/store', equipamiento).then(response => {
                console.log(response.data)
            })
        }
    },
    mutations: {
        SET_PLANES(state, planes) {
            state.planes = planes
        },
        SET_EQ(state, equipamiento) {
            state.equipamiento = equipamiento
        }
    },
    getters: {
        singleEq(state, id) {
            id => {
                return state.equipamiento.filter(eq => eq.id === id)
            }
        }
    }
})
