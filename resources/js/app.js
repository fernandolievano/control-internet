require('./bootstrap')

window.Vue = require('vue')
window.Swal = require('sweetalert2')
window.VuePaginate = require('vue-paginate')

Vue.use(VuePaginate)

Vue.prototype.$eventHub = new Vue()

import store from './store/store'

const app = new Vue({
    el: '#app',
    store,
    components: {
        Clientes: () => import('./components/Clientes.vue'),
        Equipamientos: () => import('./components/Equipamientos.vue'),
        Planes: () => import('./components/Planes.vue')
    }
})
