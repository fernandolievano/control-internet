<script>
jQuery.fn.equalHeights = function() {
    var maxHeight = 0;
    // get the maximum height
    this.each(function() {
        var $this = jQuery(this);
        if ($this.height() > maxHeight) {
            maxHeight = $this.height();
        }
    });
    // set the elements height
    this.each(function() {
        var $this = jQuery(this);
        $this.height(maxHeight);
    });
};

jQuery(window).load(function() {
    var classes = new Array("same-height", "fix-height");
    classes.each(function(className) {
        var selector = '.' + className;
        jQuery(selector).equalHeights();
    });
});
</script>