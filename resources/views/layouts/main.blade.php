<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Icons -->
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <header>
        @include('partials.navbar')
    </header>
    <div id="app">
        <main>
            <header>
                @yield('header')
            </header>
            @yield('content')
        </main>
    </div>
    <footer id="footer" class="footer has-background-dark has-text-centered">
        @include('partials.footer')
    </footer>
    @include('scripts.navbar-burguer')
    @include('scripts.modals')
</body>

</html>
