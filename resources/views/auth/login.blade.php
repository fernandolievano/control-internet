@extends('layouts.main')

@section('content')
<section class="section">
    <div class="container">
        <div class="columns is-mobile is-centered is-vcentered">
            <div class="column is-4-desktop is-4-tablet is-centered">
                <h1 class="is-size-1">Iniciar Sesión</h1>
                <div class="card">
                    <form action="{{ route('login') }}" method="POST">
                        @csrf
                        <div class="card-content">
                            <div class="content">
                                <div class="field">
                                    <label for="username">Usuario</label>
                                    <div class="control has-icons-left">
                                        <input class="input {{ $errors->has('username') ? 'is-danger' : '' }}"
                                            type="text" name="username" placeholder="Usuario"
                                            value="{{ old('username') }}" required>
                                        <span class="icon is-left">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('username'))
                                    <span class="is-size-7">
                                        <strong class="has-text-danger">{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="field">
                                    <label for="password">Contraseña</label>
                                    <div class="control has-icons-left">
                                        <input class="input {{ $errors->has('password') ? 'is-danger' : '' }}"
                                            type="password" name="password" placeholder="Contraseña" required>
                                        <span class="icon is-left">
                                            <i class="fas fa-lock"></i>
                                        </span>
                                    </div>
                                    @if ($errors->has('password'))
                                    <span class="is-size-7">
                                        <strong class="has-text-danger">{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <div class="card-footer-item">
                                <div class="field">
                                    <div class="control">
                                        <button type="submit" class="button is-primary" name="button">Iniciar
                                            Sesión</button>
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
