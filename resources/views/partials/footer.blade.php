<div class="content">
    <p class="has-text-blue has-text-weight-bold is-size-7">
        Desarrollado por Fernando Liévano
    </p>
    <p>
        <a href="https://bulma.io">
            <img src="{{ asset('img/made-with-bulma--white.png') }}" class="bulma-img" alt="made with Bulma">
        </a>
    </p>
</div>