 <nav class="navbar is-dark" role="navigation" aria-label="navigation">
     <div class="navbar-brand">
         <a class="navbar-item" href="/home">
             <i class="fas fa-home"></i>
         </a>
         <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbar">
             <span aria-hidden="true"></span>
             <span aria-hidden="true"></span>
             <span aria-hidden="true"></span>
         </a>
     </div>
     <div id="navbar" class="navbar-menu">
         <div class="navbar-start">
             <a href="{{ route('clientes.index') }}" class="navbar-item">
                 <span class="icon has-text-link">
                     <i class="fas fa-users"></i>
                 </span>
                 <span>Clientes</span>
             </a>
             <a href="{{ route('planes.index') }}" class="navbar-item">
                 <span class="icon has-text-info">
                     <i class="fas fa-wifi"></i>
                 </span>
                 <span>
                     Planes
                 </span>
             </a>
             <a href="{{ route('equipos.index') }}" class="navbar-item">
                 <span class="icon has-text-primary">
                       <i class="fas fa-hdd"></i>
                 </span>
                 <span>Equipamiento</span>
             </a>
             <a href="#" class="navbar-item">
                 <span class="icon has-text-success">
                     <i class="fas fa-money-check-alt"></i>
                 </span>
                 <span>Tickets</span>
             </a>
         </div>
         <div class="navbar-end">
             @if (Auth::check())
                <div class="navbar-item has-dropdown is-hoverable">
                    <div class="navbar-link">
                        Opciones
                    </div>
                    <div class="navbar-dropdown is-right">
                            <a class="navbar-item" href="#">
                                <span class="icon is-small">
                                    <i class="fas fa-cog"></i>
                                </span>
                                <span>Ajustes</span>
                            </a>
                            <a href="{{ route('logout') }}" class="navbar-item"
                                onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                <span class="icon is-small">
                                    <i class="fas fa-sign-out-alt"></i>
                                </span>
                                <span>
                                    Cerrar sesión
                                </span>
                            </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
             @endif
         </div>
     </div>
 </nav>
