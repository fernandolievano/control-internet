@extends('layouts.main')

@section('header')
<div class="hero is-dark">
    <div class="hero-body">
        <div class="container">
            <h1 class="title">Control de clientes wi-fi</h1>
            @unless (Auth::check())
            <h2 class="subtitle">Inicio de sesión</h2>
            @endunless
        </div>
    </div>
</div>
@endsection

@section('content')
<section class="section">
    <div class="container">
        <div class="columns is-mobile is-vcentered is-centered">
            <div class="column is-5-desktop is-5-tablet">
                @if (Auth::check())
                <a href="/home" class="button is-large is-fullwidth is-primary is-outlined">
                    <span class="icon is-medium">
                        <i class="fas fa-sign-in-alt"></i>
                    </span>
                    <span>
                        Ir a la página principal
                    </span>
                </a>
                @else
                <div class="card">
                    <form action="{{ route('login') }}" method="POST">
                        @csrf
                        <div class="card-content">
                            <div class="content">
                                <div class="field">
                                    <label for="username">Usuario</label>
                                    <div class="control has-icons-left">
                                        <input class="input" type="text" name="username" placeholder="Usuario">
                                        <span class="icon is-left">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="field">
                                    <label for="password">Contraseña</label>
                                    <div class="control has-icons-left">
                                        <input class="input" type="password" name="password" placeholder="Contraseña">
                                        <span class="icon is-left">
                                            <i class="fas fa-lock"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <div class="card-footer-item">
                                <div class="field">
                                    <div class="control">
                                        <button type="submit" class="button is-primary" name="button">Iniciar
                                            Sesión</button>
                                    </div>
                                </div>
                            </div>
                        </footer>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
